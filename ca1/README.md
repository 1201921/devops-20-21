# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation


### 1.1. Creation of a Repository

Initially a bitbucket repository was created with the name "DevOps-20-21-<1201921-Luis_Belo>".

In the first class an example repository was given to the students. It is located in the [teacher's repository](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/). A clone of it was made to my local machine by using several git commands.

- Select the folder were to past the files, in my case was `cd c/Users/luis_/repos`


- Clone the repository `git clone https://1201921@bitbucket.org/atb/devops-21-rep-template.git`


- Get inside the folder, new files were pushed in to my new bitbucket repository (created previously). To do that I used the next command `git push --mirror https://1201921@bitbucket.org/1201921/devops-20-21.git`

In the end of the class I made the assignment that consisted of adding the "jobTitle" field to the application. Note that in this class weren't used branches, so the addition of this new functionality was performed in the master branch.

---

### 1.2. Adding "Email-field" branch to the Repository


As indicated, a new branch must be created when introducing a new functionality in our application as the master branch should only have the final versions of the application. To follow this guideline I executed the next commands:

- Get in the repository folder `cd c/Users/luis_/repos/devops-20-21/`


- Create a new branch named "Email-Field" `git branch Email-Field`

By using the command «**git branch -v**» I can see that the branch was created, but I'm currently working on the master one.

![img.png](images/img.png)

- Checkout to the branch I just created using `git checkout Email-Field`

A message was shown confirming the operation. Using the same command as before I can see I've switched to "Email-Field" branch

![img_1.png](images/img_1.png)

Note that I could use the command «**git checkout -b Email-Field**» to enclose in only one command what was executed in the last two steps.

At this point, if the command `git status` is used the status of the folder will be presented as follows.

![img_2.png](images/img_2.png)

I want to make a commit with all the changes made so far.

- Add all changed files from my working directory to the staging area using `git add .` or a specific file using `git add README.md` for example.


- Commit such files with `git commit -m "message"`


- Push changes to Email-Field branch with `git push origin Email-Field`. If I would like to push to master branch I would write "master" instead of "Email-Field".

Now I can see, by using the command `git log --oneline --decorate --graph --all
`, that Email-Field is two steps ahead of master branch
Note: due to an error I had to make two commits instead of only one.

![img_3.png](images/img_3.png)

If I want to merge the information in the current branch to the master I should checkout to master and pull information from Email-Field branch, using the following commands `git checkout master` and `git merge Email-Field`.

In the next chapter I will adapt the application to display the email of the employee. So let's tag this stage as "v1.2.0".

- Using the command `git tag -a v1.2.0 -m "message" <commit number>`, where message is "JobTitle" and commit number "92392b62338fbcb24fd9e6300fd6d75e594223b7", I give an alias to the specified commit


- Command `git show v1.2.0` will present something like the following, with information about the commit with that tag

![img_4.png](images/img_4.png)


If I want to merge the information in the current branch to the master I should check out to master and pull information from Email-Field branch, using the following commands `git checkout master` and `git merge Email-Field`. 


Note that the tag can be deleted. For that purpose should be used the command `git tag -d v1.2.0`.


- To push use `git push origin master --tags`. If by mistake I insert an existing tag an error will occur when trying to push it

![img_5.png](images/img_5.png)


If for some reason I realize the tag name was incorrect I could use `git push origin :refs/tags/<tag name>` to delete it and then would repeat the previous process of creating a tag.

---

### 1.3. Adding "Email" field to the Application

To proceed I will checkout to the Email-Field branch using the command used previously, as I'm going to introduce a new functionality.

- Use the command `git checkout Email-Field`

Several changes has to be made to complete this feature, they are:

- In [Employee Class](tut-basic/src/main/java/com/greglturnquist/payroll/Employee.java) I added the following:

    * "String email" attribute, corresponding addition in the constructor and validation (at this point only validates null or empty)
    * Updated equals(), hashCode() and toString()
    * Added getEmail() and setEmail();


- In the [JavaScript File](tut-basic/src/main/js/app.js) the next parts where added:

    * Added the Header cell "Email" that compute a new collum
    * Added the data to fill in the new collum


- In the [DataBaseLoader.java](tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java) file the following were appended to the two lines of new employees from previously lessons:

    * String email as the picture shows
  

  ![img_6.png](images/img_6.png)

At this point, I want to commit the conclusion of this chapter. Note that previously I already created in the repository an [issue](https://bitbucket.org/1201921/devops-20-21/issues/1/email-field) named "Email-Field", so, I can reference it in my next commit.

- `git add .`


- Use `git commit -a "Conclusion chapter 1.3 - Adding Email field - ref #1` to add and commit all changed or new files from my working directory without passing through the staging area like I did before

- Push changes with `git push origin Email-Field`

Now I can see master branch is overdue by one step when compared with Email-Field branch

![img_7.png](images/img_7.png)

---

### 1.4. Adding unit tests for class Employee Class

The next tests were added.

```java
class EmployeeTest {
    @Test
    public void testingConstructorWithEmptyEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "");
        });
    }

    @Test
    public void testingConstructorWithNullEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", null);
        });
    }

    @Test
    public void testingConstructorWithEmailFieldFilled(){

        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "abc.pt");
        Employee employee2 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "abc.pt");

        assertNotNull(employee1);
        assertEquals(employee1, employee2);
    }
}
```
This concludes my functionality and for all I know it is correct, running great, and it won't bring future problems to the application. Now, I can commit  my changes, close issue #1 and push to the master branch.

- `git add .`

  
- `git commit -m "Conclusion chapter 1.3 - Adding Email field - close #1"` to commit and close issue


- `git push origin Email-Field`


- `git tag -a v1.3.0 -m "Conclusion chapter 1.3 - Adding Email field - close #1" 79d83f9` to tag last commit


- `git push origin v1.3.0` to push tag


- Checkout to the master branch with `git checkout master`


- Merge branches using `git merge Email-Field`


- Using `nano README.md` I will resolve all conflicts of the merging then make a new commit to merge information


- For last add modified README.md file, commit and push

Now fix-invalid-email branch can be deleted locally and remotely using `git branch -d Email-Field` and `git push origin --delete Email-Field` respectively.

---

### 1.5. Solving bug, adding pattern check and corresponding tests

Initially I was checking email for empty or null state. Obviously, this could lead to incorrections in the email format. To rectify this case, a check format should be implemented. A new [issue](https://bitbucket.org/1201921/devops-20-21/issues/2/fix-invalid-email) was created in bitbucket.

First, I should create a new branch

- `git checkout -b fix-invalid-email` to create and checkout automatically to the new branch

Now I can proceed with the changes. In Class Employee I added the following 

![img_8](images/img_8.png)

The test file has now the following tests

```Java
class EmployeeTest {
    @Test
    public void testingConstructorWithEmptyEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "");
        });
    }

    @Test
    public void testingConstructorWithNullEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", null);
        });
    }

    @Test
    public void testingConstructorWithEmailFieldFilled(){

        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "abc@abc.pt");
        Employee employee2 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "xwz@abc.pt");

        assertNotNull(employee1);
        assertNotEquals(employee1, employee2);
    }

    @Test
    public void testingConstructorWithEmailFieldWithoutAtSign(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email.pt");
        });
    }

    @Test
    public void testingConstructorWithInvalidDomainEmailField(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email@abc.p");
        });
    }

    @Test
    public void testingConstructorWithEmailFieldWithTwoAtSigns(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email@email@abc.pt");
        });
    }
}
```

At last, I fixed my bug. Using `git status` I can see I've changed three files and created a new one.


![img_9](images/img_9.png)

- `git add .` to add changes to staging area


- `git commit -m "Conclusion chapter 1.4 - Solving email bug - close #2"` to commit and close issue #2


- `git push origin fix-invalid-email` to push


- To tag new version of the program used `git tag -a v1.3.1 -m "Conclusion chapter 1.4 - Solving email bug - close #2" 02452b0ae8d91b583ea0e3d7d433037327ffcffa` 

- `git push origin v1.3.1` to push tag

Now my work is ready to merge with master branch

- Checkout to master `git checkout master`


- `git merge fix-invalid-email` to merge information


Now fix-invalid-email branch can be deleted locally and remotely using `git branch -d fix-invalid-email` and `git push origin --delete fix-invalid-email` respectively.


---

### 1.6. Debug program

#### 1.6.1. IDE

At any point of the implementation process I could use the debug tool on my IDE (IntelliJ) if needed. To perform a debug to the server I should create a breakpoint as shown in the next image and run the program on debug mode

![img_10](images/img_10.png)
(just left click on the place where is located the red dot)

![img_11](images/img_11.png)

Then, I would go step by step to understand the reason that let me debug the server.

![img_12](images/img_12.png)

#### 1.6.2. Front End

In Windows OS, with the application running and using Google Chrome, I can debug the front end of my application. For that matter, with the application page open, I should do _Ctrl+Shift+I_ or go to _Options > More tools > Developer Tool_. Something similar to the next image will appear.

![img-13](images/img_13.png)

In it, I already have a breakpoint flagged in blue in line 6. Just simply left click on any line number to create more breakpoints. Now if I refresh the page the program will stop in my first breakpoint. 

![img-14](images/img_14.png)


In the up right corner I could progress step by step, or resume program at any moment. 

![img-15](images/img_15.png)

---


## 2. Analysis of an Alternative

Git is a free and open source distributed version control system created in 2005 by Linus Torvalds, it is widely used and, probably, the kind of system version control with most weight nowadays. 

There are others options, one of them is Mercurial, created also in 2005, that started to be very concurrent and had a lot of prevalence, but has been losing its power over the years. Similar to Git, Mercurial is a free distributed version control system. This means that there is a central repository and each developer has its own local repository with his working directory in which is working at. Later, when his/her task is done, he/she can merge information with the central repository, allowing others to access the new information.  From the feedback I've been reading, Git and Mercurial are very similar in their essence, but Mercurial is easier for beginner users.

As with Git, Mercurial stores a series of snapshots but in its case they are called ChangeSet. Each one is uniquely identified, and each ChangeSet has a parent (except the first). So, in each commit a ChangeSet is stored containing changes made (diffs) and not the all repository. In the case of Mercurial ChangeSets are immutable.

At syntax level, Mercurial is simpler, but for expert users Git is more indicated because provides a wider range of tools, therefore, becoming more flexible. 

When talking about security, I don't yet have experience to produce a proper opinion, so my text will rely on only what I read. Because Git is a more complex tool than Mercurial, a user needs to know it deeply in order to safely and effectively use it. 

About branching, Mercurial embeds the branches in the commits, where they are stored forever. This means that branches cannot be removed because that would alter the history. In the case of Git branches are only references to a certain commit, and I can create, delete, and change a branch anytime, without affecting the commits. Other thing different from Git is that changes are committed as they are in the working directory. There is no staging area like in Git, so every modified files (at that time) will be included in the same commit.

In conclusion, Mercurial is not so different from Git. Their structure, commands and other things are really similar between them. Mercurial even has a file called _.hgignore_ with a list of types of untracked files like in Git. Although Git has become an industry standard, which means more developers are familiar with, Mercurial seems to be a very valid choice of VCS.

---
 
## 3. Implementation of the Alternative

### 3.1. Creation of a Repository

First a repository was created through Helix Team Hub, with the name devops-20-21_Mercurial-_1201921-luis_belo. 

As said before, in the first class an example repository was given, located in the [teacher's repository](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/). I have downloaded it to my machine.

- Selected the folder were to clone my repository with `cd C:\Users\luis_\repos\`


- Clone repo `hg clone https://luis_belo91hotmailcom@helixteamhub.cloud/wandering-eye-4364/projects/devops-20-21-_1201921-luis_belo/repositories/mercurial/devops-20-21_Mercurial-_1201921-luis_belo`


- Paste teacher's repo to mine using the explorer


- Use `hg add` to add all new files


- Commit such files using `hg commit -m "First commit"`


- Push changes with `hg push`. A password account will be requested. 
  

If like me is the first time using Mercurial, it will also ask to define a username. I did that by using the command `hg config --edit`. In Windows OS a config file will open in notepad. In it is possible to config several definitions but for now the username and email is what I want to define.

![img_18](images/img_18.png)

Then, added the assigment for the first class, that was adding JobTitle, add files, commit and push, as in the last three steps. 

- Using `hg log` I can view a list of commits as follows

![img_16](images/img_16.png)

Note that at the moment this capture was made I had already created the tag v1.2.0 with the command `hg tag -r 1:a51645a766e8 v1.2.0`. In Mercurial tags are stored in a file called ".hgtags". In it each line corresponds to a tag, and is composed by a ChangeSet number and its corresponding tag. It can be removed with `hg tag --remove <tag name>`.

![img_19](images/img_19.png)


Using `hg log` again we can see the result.

![img_17](images/img_17.png)

Last ChangeSet was applied automatically by Mercurial. It has the tag "tip" that is considered a special "floating" tag, meaning that identifies the newest revision in the repository. This will always be the first tag listed.

---

### 3.2. Adding "Email" branch to the repository

At this point I'm ready to add email field to the application. First, I have to create a new branch called "Email-Field" where to make the changes.

- Use `hg branch Email-Field` to create the branch. This branch will only appear in the repository after the next commit


I'm already in the branch I just created, this means, there is no need to checkout like in Git. After make a small change in any file (in my case added one line in Employee.java) 

- Don't need to use `hg add` because no new files were created
  

- Commit changes using `hg commit -m "Added Email-Field Branch`


- And push with `hg push`


Using `hg branches` I can see what branches were created and their state. For example at this moment there are two branches, and the master(default) is inactive. Branch Email-Field is two commits ahead of default one.

![img_21](images/img_21.png)

---

### 3.3. Adding "Email" field to the Application and unit tests to Employee class

The changes to achieve the functionality are the same as the ones explained in chapter 1.3, for that reason I will not repeat them again. After the changes were made I have to commit them. At this moment if I use the command `hg status` the follow information will be retrieved

![img_20](images/img_20.png)

In it, we can see in blue, with a letter "M" in the beginning, files that has been modified. In green, with a letter "A", are the files that has been added, this means, new files. For last, in red, with a letter "R", is the file that has been removed.

- Use `hg add` to add all new files, if there is any


- With `hg commit -m "Added Email-Field - close #1"`  to commit and relate it to issue number 1


- Finally, push changes using `hg push`

Now it is time to merge Email-Field branch with default branch, and tag the repository. In Mercurial merging is set by:

- First, update default branch with `hg update default`. This is the output

![img_23](images/img_23.png)


- Then, merge with Email-Field branch using `hg merge Email-Field`. The next image shows the result of this operation

![img_24](images/img_24.png)


- Commit `hg commit -m "Merging branches"`. This is the outcome

![img_22](images/img_22.png)

I can see that this commit has two (ChangeSet) parents: 2:36646f221bea » from default branch; 4:a8a02e05f5de » from Email-Field branch

- Tag changes as "v1.3.0" using `hg tag -r 5:9153d89bfc4a v1.3.0`


- Push changes `hg push`

Below, the result of my last changes is shown

![img_25](images/img_25.png)

--- 

### 3.4. Solving bug, adding pattern check and corresponding tests

Again, the process of fixing the bug is equal to the one explained in chapter 1.5. After changes were made, by using the command `hg status` the following information is shown.

![img_26](images/img_26.png)

Now the bug is fixed I can commit/push changes.

- Use `hg commit -m "Solving email bug - close #2"`. In Mercurial is impossible to delete a branch as explained in the Analysis, but is possible to close it. Therefore, I should have used `hg commit --close-branch -m "Solving email bug - close #2"`. If I used only `hg commit --close-branch` a new window would appear asking to add a commit message like the next image shows

![img_27](images/img_27.png)

- `hg push`

Now it is time to merge changes and tag repository.

- Update default branch with `hg update default`


- Merge with Fix-Invalid-Email branch using `hg merge Fix-Invalid-Email`


- Tag changes as "v1.3.1" using `hg tag -r 11:8a62c2ef9de5 v1.3.1`


- Push changes with `hg push`

--- 

### 4. Tag Bitbucket Repository

It was told to students that they should tag their repository with the tag "CA1" in the end of the assignment.

- `git add .`


- `git commit -m "Conclusion of the Class Assignment number 1"`


- To tag final version use `git tag -a CA1 -m "Conclusion of the Class Assignmente number 1" b5975ce163fdaa98c01aa9cdf8c9ab7852a91448`

- `git push origin CA1` to push tag

- `git push origin master`

Job Done!! 