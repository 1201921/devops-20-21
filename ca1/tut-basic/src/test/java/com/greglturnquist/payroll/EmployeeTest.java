package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    public void testingConstructorWithEmptyEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "");
        });
    }

    @Test
    public void testingConstructorWithNullEmail(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", null);
        });
    }

    @Test
    public void testingConstructorWithEmailFieldFilled(){

        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "abc@abc.pt");
        Employee employee2 = new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "xwz@abc.pt");

        assertNotNull(employee1);
        assertNotEquals(employee1, employee2);
    }

    @Test
    public void testingConstructorWithEmailFieldWithoutAtSign(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email.pt");
        });
    }

    @Test
    public void testingConstructorWithInvalidDomainEmailField(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email@abc.p");
        });
    }

    @Test
    public void testingConstructorWithEmailFieldWithTwoAtSigns(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "Mechanic", "email@email@abc.pt");
        });
    }
}