# Class Assignment 2 Report

The purpose of class assignment 3 Part1 is to practice with ubuntu via VirtualBox, using projects from last assignments.

## Virtual Machine creation/configuration

We will start by install VirtualBox in our local machine. For that matter follow this [link](https://www.oracle.com/virtualization/technologies/vm/downloads/virtualbox-downloads.html?source=:ow:lp:cpo::RC_WWMK201210P00009:LPD400008724&intcmp=:ow:lp:cpo::RC_WWMK201210P00009:LPD400008724), download and install the version best suited to your OS.

After installation is complete open the application. To create a virtual machine (VM) access "Machine" in the top left corner and click new. 

![img_1.png](Images/img_1.png)

- choose the machine name you prefer, folder where it will be located, type "Linux" and version "Oracle (64-bit)". After click next.

![img_2.png](Images/img_2.png)

- choose at least 2048mb of RAM.


- choose "Create a virtual hard disk now", then click "Create" button.


- in the hard disk file type choose "VDI", then click "Continue".


- the storage should be dynamically allocated.


- in file location and size choose 10Gb, then "Create".

Now your VM is created. You can see its characteristics on the right side of the window. Let configure out VM.

- To install our OS first access this [link](http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso) and download the iso file.

- Locate "Storage" on the right and click it. 

![img_3.png](Images/img_3.png)


- choose "empty", check the checkbox "Live CD/DVD" (this way the disk will be loaded on system boot), click the disk icon, choose "choose a disk file...", then select the iso file you just downloaded.

![img_4.png](Images/img_4.png)

Now in the optical drive you can see the ubuntu cd.

![img_5.png](Images/img_5.png)


For our assignment we still need to create two Network adapters. One is a Nat type and allows the VM to access Internet, the second is virtual local network.

- In the top left corner select file > Host Network Manager. Then click on "Create" icon and check the enable checkbox of "VirtualBox Host-only ethernet adapter #2".

At this we are ready to start our VM and install Ubuntu. So, click the green arrow icon to start. The cd present in the optical drive will be loaded. A new window will pop-up.

- select install.

- choose the language.

- let the machine detect your keyboard, and follow the instructions.

- choose the "enp0s3" as primary network interface.

- insert the computer name you prefer.

- choose the mirror of ubuntu archive. Portugal works.

- don't select any proxy.

The program will now download the necessary software.

- define your user complete name, account username and password.

- select your time zone.

- in the type of partitioning choose "guided - use the entire disk". This will format the disc and use all virtual disc to install ubuntu.

- select the disc then permit the writing.

Now, the OS will be installed.

- when required choose "without automatic updates".

- select "yes" to GRUB boot manager.

Installation is now complete. We must remove the cd in the optical drive, or else, everytime we start the VM the ubuntu installation process will start. Proceed as follows in the next image.

![img_6.png](Images/img_6.png)

Now click continue on your VM. The system will reboot and credentials will be asked. Insert them.

Some more setup is needed. In the command line proceed as described below.

- Install network tools using
    
    - `sudo apt update`
    - `sudo apt install net-tools`
    - `sudo nano /etc/netplan/01-netcfg.yaml`. The content should look like below
    ``` 
    network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: yes
        enp0s8:
          addresses:
            - 192.168.56.5/24
    ```
    - `sudo netplan apply`
  
- Install openssh. This will allow opening secure terminal sessions from other hosts
    
    - `sudo apt install openssh-server`
    - `sudo nano /etc/ssh/sshd_config` and uncomment the line "PasswordAuthentication yes"
    - `sudo service ssh restart`
  
Now that this is enabled you can use, for example, your host terminal and type ssh "username"@"VM IP address". We now have access to your VM from and terminal. 
  
- Install a ftp server so that you can use the FTP protocol to transfer files from other hosts
    
    - `sudo apt install vsftpd`
    - `sudo nano /etc/vsftpd.conf` and uncomment the line "write_enable=YES" to enable write access to vsftpd
    - `sudo service vsftpd restart`
  
With FTP server enabled you can use applications like [FileZilla](https://filezilla-project.org) to easily transfer files from/to the VM. To this assigment this won't be needed.

- Install git and Java
  
    - `sudo apt install git`
    - `sudo apt install openjdk-8-jdk-headless`
  
- Another tool that can be very interesting is the "SdkMan". It allows you to quickly, and in a very simple way, install any software from a long list of programs/versions such as gradle, maven, java, etc.
  
    - `curl -s "https://get.sdkman.io" | bash` (note: you might need to install curl before)
    - `source "$HOME/.sdkman/bin/sdkman-init.sh"`
  
For example, for this assignment you will need, at least, gradle 6.6 and Maven. Using sdk is very simple to install it. Just use the command `sdk install gradle` and the latest version will be installed.

## Cloning Repository to VM

To clone our repository we need to use commands previously used.

- use `git clone git@bitbucket.org:1201921/devops-20-21.git`

Get inside the repository root folder, create ca3/Part1 and copy tut-basic (maven) and gradle_basic_demo.

- `mkdir ca3/Part1`

- `cp -a ca1/tut-basic/. ca3/Part1/maven-tut-basic`

- `cp -a ca2/Part1/gradle_basic_demo/. ca3/Part1/gradle_basic_demo`

The option `-a` is a recursive option, that preserve all file attributes, and also preserve symlinks. The option `.` at the end of the source path is a specific `cp` syntax that allow to copy all files and folders, including hidden ones.

## Using tut-basic project

Now, inside maven-tut-basic we can try to run spring boot. Use `mvn spring-boot:run` and watch the result. If every went well, as it should, use the browser in your host computer to access http://192.168.56.5:8080. Remember that we are not using localhost anymore. The reason you must use your host's browser is that this ubuntu version does not have a graphic interface, so you need to use a system that have. You should see the table with the information from previous lessons, as follows.

![img_7.png](Images/img_7.png)


## Using gradle_basic_demo

Inside gradle_basic_demo we can try to run spring boot. Use `java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`. As referred this ubuntu version does not have a graphic interface. Go head and use the command `gradlew runClient` in your host's terminal . Unfortunately, an error "Connection refused" occur. 

![img_8.png](Images/img_8.png)

This is happening because we are using the previous task runClient, that is using localhost. We must edit our build.gradle file. Use `nano build.gradle` and substitute the old "executeServerAtPort59002" task with the following code.

``` 
task runClientVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on http://192.168.56.5:59001/ "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

Now, run the task "runClientVM". Don't forget to have the chat server running. A chat window should pop-up. If you, like me, had an error in "compileJava" because there's no JDK valid installation append the next line to gradle-properties file

  - `org.gradle.java.home=C:/Program Files/Java/jdk1.8.0_261`
note: your path might be different. Look for the installation folder of java 8. 

![img.png](Images/img.png)
