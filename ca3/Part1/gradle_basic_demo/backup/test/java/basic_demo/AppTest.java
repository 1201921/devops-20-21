package basic_demo;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AppTest {

    @Test
    public void CheckGetGreeting() {
        App classUnderTest = new App();
        assertNotNull("App should have a greeting", classUnderTest.getGreeting());
    }
}