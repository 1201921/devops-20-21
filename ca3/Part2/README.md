# Class Assignment 3 Part2 Report

For this assignment students must use Vagrant to set up a virtual environment to execute the tutorial spring boot application, developed in ca2 Part2. For this purpose they should replicate teacher's instructions, altering certain parts accordingly with their repositories. 

---

## 1. Analysis, Design and Implementation

## 1.1. Vagrant Installation

You can get the installation file for Vagrant accessing this [link](https://www.vagrantup.com/downloads). Look for the appropriate file for your OS. 

Installation is easy and very straight forward.

For the next step is supposed you have VirtualBox installed in your machine. If not, see how in last assignment ([ca3-Part1](../Part1/README.md)).


## 1.2. Preparing Project

First thing you should do is clone or download [this repository](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/). In it, we will find a file called VagrantFile. Copy it to ca3-Part2 folder. This and the READMED file will be useful for our goal in the future.

Now, create and checkout to a new branch called "Vagrant-tut-basic". Open your terminal inside ca3-Part2 and use `git checkout -b Vagrant-tut-basic`.

Copy demo folder inside ca2-Part2 to ca3-Part2 using `cp -a ../../ca2/Part2/demo/. demo`

At this moment, we are ready to start changing our project.

- Go to your build.gradle file and check if you have the plugin `id 'org.siouan.frontend' version '1.4.1'`. After that, in the package.json confirm that you have the webpack script as follows
``` 
"watch": "webpack --watch -d",
"webpack": "webpack"
```
- Add the war plugin `id 'war'` to build.gradle. This will make gradle generate a war file when building the project. It contains all parts of the web application and will allow the deployment of our web application.

- In the same file, add dependency `providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'`. This tells that, in run time, there is a tomcat where the application will be running. 

- Create a new Java file called "ServletInitializer" in src/main/java/com/greglturnquist/payroll/ with the following information. This tells spring to use the external tomcat.

``` 
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder) {
        return applicationBuilder.sources(ReactAndSpringDataRestApplication.class);
    }
}
```

- Add some properties, so we can use H2 console. Go to src/main/resources/application.properties. The file should look as follows

``` 
server.servlet.context-path=/demo-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

- In React with need to perform changes too. Open app.js and change the path from get method to `'/demo-0.0.1-SNAPSHOT/api/employees'`. 

- A bug should be fixed in src/main/resources/templates/index.html. Remove the front slash before main.css in line 6

![img_1.png](Images/img_1.png)

### 1.3. Creating Virtual Machines (VM)

After we have a Vagrant file is very easy to create the machines where to run our servers. At this point, we will use VagrantFile we copied in the beginning of last chapter. Go to ca3/Part2 folder in your console and open the file. This will depend on what OS you are using. For Windows OS use `notepad Vagrantfile`. In Unix, you can use vi or nano. For the examples nano was used that is best suited to beginner users.It is possible that you need to install nano first. The looks of the editor is shown below.

![img_3.png](Images/img_3.png)

The file is preconfigured to install two VMs with all necessary tools and configurations to run servers correctly. Even so, some changes have to be made to adapt to our project.

First let's take a look at the file content. As you can observe in last image both Vm's will use the box ubuntu-xenial. A general provision section is present, that installs some base tools, for example JDK 8. Then, there are two specific sections to configure VM's, one for each. 

The part we should change is located in the end of the document.

First, we need to insert the correct link to clone our repository. After that write the project folder path. Finally, check if the war file is correct. Next image shows where to make changes if needed.

![img_4.png](Images/img_4.png)

Press ctrl+X, save the changes and exit nano.

At last, you are ready to create your VM's. Run `vagrant up`. This command will use the referred file to install VMs (database and web).

This process will take a while. If everything goes well you should get the next output. 

![img_5.png](Images/img_5.png)

In VirtualBox two VM's (db and web) were created. The two should be running, so you get something similar to next image.

![img_6.png](Images/img_6.png)

### 1.4. Accessing Web Application and H2 console

Now your Spring Boot Application is running. You can access it! 

Open a browser window on your host machine and use one of option below.

- To open the web application:
    
    - http://localhost:8080/demo-0.0.1-SNAPSHOT/
    - http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/

This is what should be presented to you.

![img.png](Images/img.png)
    
- To open H2 console: 
    
    - http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
    - http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
    
    **note:** you should use **jdbc:h2:tcp://192.168.33.11:9092/./jpadb** in "JDBC URL" field

After connect, select "EMPLOYEE" on the left menu, click "Run", and you should get the next output.

![img_2.png](Images/img_2.png)

---

## 2. Analysis of an Alternative

First off all, VirtualBox is a hypervisor, i.e., in general terms, a hypervisor able the user to install, test and use different OS over the real (host) OS, without the risk of change it or affect our data. There are two types: type 1 and type 2. The first type means the hypervisor runs directly on the computer's hardware, taking control from BIOS or UEFI when the host OS boots, managing operating system. Type 2, that is the case of VirtualBox, is a hosted hypervisor. VirtualBox, or other type 2 hypervisor, is an application that runs on the host OS. Only when the user starts the program the VM can be started, then VM hosted processes are created. I chose Hyper-V as a study case of alternative to VirtualBox, a type 1 free hypervisor from Microsoft. Due to its characteristics, Hyper-V is always on if the host is powered on, while the VirtualBox can be started and closed by a user on demand.

Next is presented a summary table with characteristics from each hypervisor. 

|                       | **VirtualBox**    |  vs   | **Hyper-V**|
| :---:                 | :---:   | :---:   | :---:   |
|**Manufacture**        | Oracle      |     | Microsoft      |
|**Cost**               | Free  |       | Free      |
|**Hypervisor Type**    | 2  |       | 1       |
|**Host OS**            | Windows, Mac, Linux, etc  |       |Windows 8 onwards      |
|**Guest OS**           | All from Hyper-V and much more  |       |Linux, FreeBSD, and Windows|
|**Vagrant**            | Works well | | Works with some limitations* |
|**Snapshots/Checkpoints**          | Snapshot - Allow  |       | Checkpoint (similar to snapshot) - Allow (often raise errors)     |
|**Working Privileges** | Doesn't need |  | It's a must to use Vagrant |
|**Sharing folders/files** | Allow (built in feature - giving access similarly to HyperV; or drag & drop) | | Allow (done manually providing access to a folder to specific users/groups; through Copy-VMFile PowerShell tool - copy + paste; or drag & drop)

*Limited networking using vagrantfile » "Vagrant does not yet know how to create and configure new networks for Hyper-V"; "networking configurations in the Vagrantfile are completely ignored with Hyper-V. Vagrant cannot enforce a static IP or automatically configure a NAT." (excerpt from [Vagrant website](https://www.vagrantup.com/docs/providers/hyperv/limitations))

In conclusion, if a person or enterprise is using Windows on physical machines the choice could go to Hyper-V. If several platforms are being used then the preference should fall to VirtualBox. For me VirtualBox is more flexible, has more pros and fewer cons when comparing to Hyper-V. When using Vagrant, as we did, VirtualBox is the best choice, due to Hyper-V limitations. Nevertheless, Hyper-V worked well as an alternative, and didn't require so much time as other did in previous assignments. 

---

## 3. Implementation of the Alternative

### 3.1. Hyper-V installation

To install Hyper-V in our machine create a file, anywhere, using a text editor and copy the following into that file:

![img_15.png](Images/img_15.png)

Now save that as hv.bat from your text editor anywhere you want. 

Run the file as administrator and reboot the system when required. 

### 3.2. Preparing Project

We will base our alternative project on the original one. So, start by copying "demo" folder and Vagrantfile we used with VirtualBox. Paste them to a new folder called "alternative" inside ca3/Part2.

Now we will install the box that will be used with Hyper-V. Run command line as administrator (as referred before, is a must with hyper-v).

- Use `vagrant box add generic/ubuntu1604`. Choose hyper-v as provider.

![img_7.png](Images/img_7.png)

When installation is complete, use `vagrant box list` to see what boxes you have available. You should get a result similar to next image.

![img_8.png](Images/img_8.png)

At this point, we need to perform some changes in the vagrantfile we copied. Edit it through the explorer GUI or in the command line by using `notepad Vagrantfile`. Its content should be as follows. Save changes after you finish editing.

![img_16.png](Images/img_16.png)

Major changes were made in line 4 and 6 (different box and new provider - this could not be necessary but works to enforce vagrant), and different path to project. Note that as referred in the analysis of the alternative chapter, Hyper-V has a limitations regarding the use of Vagrant, that is limited networking using vagrantfile. This means that even though we configured ip networks for each VM, it will be ignored by Hyper-V. Only after the creation of VM's we will know the ip address assigned (automatically) to them, and could, successively, perform another necessary change. 

### 3.3. Creating VM's

Execute `vagrant up`. You should get the next output.

![img_9.png](Images/img_9.png)

It is possible to see that the build was successful and two VM's were created in Hyper-V.

### 3.4. Accessing Web Application and H2 console

Now your Spring Boot Application is running, but you can't access it yet! If you try using localhost or the ip addresses we had in Vagrantfile you will get the next output.

![img_13.png](Images/img_13.png)

This happens because what I referred in the end of last chapter. Compare your VM's ip addresses with the ones you have in the Vagrantfile.

![img_14.png](Images/img_14.png)
(note: your actual IP addresses will certainly be different)

So, what do we need to do to make our Web Application and H2 console accessible? 

- In the terminal use `vagrant ssh web` to access the web VM.

- Navigate to devops-20-21/ca3/Part2/alternative/demo/src/main/resources and use `nano application.properties`. Alter the IP address in line 4 to your db VM IP address (same port).

- Save changes using ctrl+X. 

- Run build task with `./gradlew clean build`

- Use `exit` to log out.

Now, open a browser windows in your host OS and try accessing. 

- Web Application in available through the url « < Your web IP Address >:8080/demo-0.0.1-SNAPSHOT/ ». 
  
- H2 console is accessible through the url « < Your web IP Address >:8080/demo-0.0.1-SNAPSHOT/h2-console ». In "JDBC URL" field insert « jdbc:h2:tcp://<your db IP Address>:9092/./jpadb ».

### 3.5. Other considerations

In the end of my assignment, after I had everything working (web application and h2 console), I tried to update database, and changed the two entries I had. This is what it looked like in DatabaseLoader.java file.

![img_11.png](Images/img_11.png)

As is possible to observe there are two entries.

Then ran two commands:

- New build with `./gradlew clean build`

- Copied war file with `sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps`

When accessing web application and h2 console the new entries were appended, as is possible to see next.

![img_12.png](Images/img_12.png)

Didn't understand exactly why this is happening. Tried to investigate the reason but without success. I suspect that it is related with the property we used not to lose data from the database.