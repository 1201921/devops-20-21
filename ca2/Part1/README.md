# Class Assignment 2 Report - Part 1

In this second assignment students should do two reports. This is the first part of the class assignment. Generally speaking, students must add to the given project three tasks: execute the server, copy source files to a backup directory and copy to a zip file the source files.

Inside CA2 folder I created two directories (Part1 and Part2). Also add a readme file inside each one. Finally, pasted [gradle basic demo](https://bitbucket.org/luisnogueira/gradle_basic_demo/) to Part1 folder. 

This "is a demo application that implements a basic multithreaded chat room server."

All project was done without installing Gradle, because it has a gradle wrapper. "As a result, developers can get up and running with a Gradle project quickly without having to follow manual installation processes saving your company time and money." (extract from https://docs.gradle.org/current/userguide/gradle_wrapper.html). Even though the installation is not mandatory, if the user wants to use gradle he must install version 6.6. The commands for gradle are the same as gradle wrapper excepting the "w" in "gradlew" ( example: where I use `gradlew build` it should be used `gradle build`).

First, I will check if is possible to build the project, and no errors occur. For that matter, I used the Windows command line prompt and introduced the next command to get inside the folder `cd C:\Users\luis_\repos\devops-20-21\ca2\Part1\gradle_basic_demo`.

Now let's build the project using `gradlew build`. If everything went well the next information will result

![img.png](Images/img.png)

In my case, because it was not the first build I made, all tasks show up as "up-to-date". This is a feature, that gradle uses to execute faster builds, i.e., from build to build gradle only recompile what has been modified.

At this point, I can run the server using `java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`. Probably, it will be necessary to give permission to execute the task. As next image shows, the servers runs.

![img_1.png](Images/img_1.png)

In another terminal, inside gradle basic demo folder, if `gradlew runClient` the next window will pop-up. 

![img_2.png](Images/img_2.png)

After choosing a name, press "ok" and enter the chat room.

It is possible to see in the previous terminal window information about the chat room.

![img_3.png](Images/img_3.png)
![img_4.png](Images/img_4.png)

Now I will introduce a task to execute the server. In Bitbucket I created an [issue](https://bitbucket.org/1201921/devops-20-21/issues/3/add-task-execute) related to this job (issue n. 3).

## 1. Add task to execute server

Was ordered to students to add a new task that execute the server. After that job a unit test should be added, and the gradle script updated to run it.

- Open "build.gradle" file. Add the next content in the end of it.

```groovy
String message = "Server will open at port 59002"

task executeServerAtPort59002 (type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Starts the server at port 59002"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59002'

    println(message)
}
```

This will run main method from "ChatClientApp", that requires one argument, in this case the number of port (59002). 

Running on IDE:

![img_5.png](Images/img_5.png)

Or in the terminal using `java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59002` from the project's root directory:

![img_11.png](Images/img_11.png)

If, for some reason, a number of arguments different from one are passed, task will run, show an error and terminate, as shown next.

![img_6.png](Images/img_6.png)


Next step is to insert test dependency. As told by the teacher, unit tests require junit 4.12 to execute. Inside "build.gradle", search for dependencies and add the `implementation 'junit:junit:4.12'` as follows

![img_7.png](Images/img_7.png)

Finally, to add a test to "App" is necessary to create the test folder according to convention, i.e., the file test has to be located in src > test > java > basic_demo. Java folder has to be marked as "Test Sources Root". Right click in it and mark it as next image shows.

![img_8.png](Images/img_8.png)

To create the test file there are two options: a) create a raw class file inside src > test > java > basic_demo and insert all the necessary code; b) open [App.java](gradle_basic_demo/src/main/java/basic_demo/App.java), right click > Generate > Test or _Alt+Insert_ > Test. Then proceed selecting the right check box, as image shows, then pressing ok. 

![img_9.png](Images/img_9.png)

The final file test should have the next code: 

```java
package basic_demo;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AppTest {

    @Test
    public void CheckGetGreeting() {
        App classUnderTest = new App();
        assertNotNull("App should have a greeting", classUnderTest.getGreeting());
    }
}
```

Now, in the terminal, to verified if the test runs
- build the project again with `gradlew build`
  
- run the command `gradlew clean test` will delete previous build’s generated output and generate it again. The option without "clean" can be used too. Bear in mind that "clean" option will cause significant additional build time for task execution.

![img_10.png](Images/img_10.png)

For a full visual report the command `gradlew clean test --scan` can be used.

---


## 2. Add task Copy source files

Student's second assignment was to add a task that copy the content of "src" folder into a backup directory. For that matter I appended the next code to "build.gradle".

```groovy
task CopySourceFiles (type: Copy) {
    group = "DevOps"
    description = "Copies the content of src folder into backup directory"

    from('src')
    into('backup')
}
```

What this segment is doing is adding a task to DevOps tasks that copy all content of src directory, in the first execution will create a folder with name "backup", then paste files to it. At next executions, this task will only override the content of backup folder.

- Execute `gradlew tasks` and is possible to see that the task is now available to execute as next image shows

![img_13](Images/img_13.png)

- Using command `gradlew CopySourceFiles` executes the task. The image that follows shows the before and after task execution

![img_12](Images/img_12.png)
![img_14](Images/img_14.png)

---

## 3. Add task Zip source files

In the third, and last assignment, was ordered the students to add a task to make an archive, i.e., a zip file of the
sources of the application. Once again, was appended a segment of code to "build.gradle" as follows.

```groovy
task zipSourceFiles (type: Zip){
    group = "Devops"
    description = "Creates a Zip file with src folder content"

    archiveFileName = 'src.zip'
    destinationDirectory = file('zip-Archive')
    from ('src')
}
```

This adds the task ZipSourceFiles to Devops group tasks as the next image shows after executing `gradlew tasks`. 

![img_15.png](Images/img_15.png)

It creates the directory "zip-Archive" in the first execution, and a zip file named "src.zip" inside. At next executions, this task will only override the zip file.

- The image below shows the before and after of the use of command `gradlew ZipSourceFiles` 

![img_16.png](Images/img_16.png)
