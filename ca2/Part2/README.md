# Class Assignment 2 - Part2 Report

For this assignment students were instructed to convert the basic version of Tutorial Application to Gradle, instead of Maven, and to analise one alternative. 

During the development of Part1 a folder named "Part2", and a ReadMe file were created. Now, inside Part2 there is a directory called "demo" where a Gradle Tutorial Application is implemented, and other with the name "Ant_Alternative" to develop an alternative to Gradle.

In all project was used gradle wrapper. To have more information about it please read [ReadMe](../Part1/README.md) introduction to Part1.

---

## 1. Analysis, Design and Implementation

Let start by creating a new branch. For that open the terminal console and navigate to Part2 folder. Now use the command `git checkout -b tut-basic-gradle`.

Go to [Spring Initializr](https://start.spring.io/) page and select fields as the next image shows.

![img.png](Images/img.png)

Click "Generate" and download the zip file to "demo" folder. Unzip it.

Delete "src" directory from the unzipped file and copy "src" folder, "package.json" and "webpack.config" from [CA1](../../ca1/tut-basic), and paste them to "demo". Finish this part by deleting "built" folder in src > main > resources > static. The reason for this is that this folder should be generated from the javascript by the tool webpack.

If the application is experimented an empty/blank page will be shown. Try it using `gradlew bootRun`. This happens because the plugin for dealing with the frontend code is missing.

To solve this problem we will add the plugin « id 'org.siouan.frontend' version '1.4.1' » to "plugins" and the code to configure it.

![img_1.png](Images/img_1.png)

The scripts section/object in "package.json" should be also updated. Just add the highlighted line on the next image.

![img_2.png](Images/img_2.png)

Build the project using `gradlew build`, followed by `gradlew bootRun`to run the application. Now our program is working. If "http://localhost:8080/" page is opened, we can see that the black page disappear, giving place to the information that we inserted in [DataBaseLoader](demo/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java) class. 

At this point, we can merge the branch "tut-basic-gradle". 
- Push changes using git commands referred in previous assignments
  
- checkout to master branch

- merge branches using `git merge tut-basic-gradle`

---

### 1.1. Add task to copy Jar

Was ordered to students to add a task that copy the "demo-0.0.1-SNAPSHOT.jar". For that matter an issue should be created in bitbucket with the name "CopyJarInGradle". Note: in my case, by forgetfulness, the issue was not created.

To accomplish this feature, we have to append the next code in the "build.gradle" file.

```groovy
task copySnapshotJar (type: Copy) {
	from 'build/libs/demo-0.0.1-SNAPSHOT.jar'
	into 'dist'
}
```

In the console, if we type the command `gradlew tasks --all` we will se that our task is now available.

![img_3.png](Images/img_3.png)

Try the new task we just add. Use the command `gradlew copySnapshotJar`. Next is the before and after view of the packages in our project. 

![img_4.png](Images/img_4.png)

Now we can commit and push changes as we have seen before. Don't forget to refer and close issue with the key "resolve" (for example, there are other words that have the same result), and the issue number. 

---

### 1.2. Add task to delete Webpack files

The last assignment was to create a task to delete generated webpack files. Those files are located in the "built" folder as referred before. Note that this task should be executed automatically by gradle before the task **clean**. For that purpose, clean task must depend on the one we are creating.  

Once again, was appended a segment of code to "build.gradle" as follows.  

```groovy
task deleteWebpackFiles (type: Delete){
	description = "Deletes content of built folder"
	delete fileTree('src/main/resources/static/built')
}

clean.dependsOn(deleteWebpackFiles)
```

Using the command `gradlew tasks --all` we can see the availability of this new task. 

![img_5.png](Images/img_5.png)

Now if we use the command `gradlew --console=plain clean` we can see the next output.

![img_6.png](Images/img_6.png)

It is possible to confirm that the task **deleteWebpackFiles** is occurring before **clean**. By looking to our packages again we confirm that "build" folder, and the content of "built" were deleted, by clean and deleteWebpackFiles task respectively. Let see the before and after again.

If, for some reason, we want to test it again we must run build command before. This is the command that will create the build folder, and the webpack files.

![img_7.png](Images/img_7.png)

Finally, we can commit and push changes as we did before, closing the issue for this task.

---

## 2. Analysis of an Alternative

For an alternative I choose Apache Ant.

Gradle, released in 2012, is a free and open source dependency management and build automation tool, a hybrid of Ant, Maven and ivy (i.e. can make everything they do). Is based on groovy domain specific language but also supports others. This tool is considered a simpler and noticeable to human eye programming language. It assists in various development operations in the building, testing, and deploying software on different platforms. Also, it has built-in plugins for Java, Scala, Android, C / C ++, Web and OSGi. Finally, Gradle provides integration with several development tools and servers, including Eclipse, IntelliJ, Jenkins, and Android Studio.

On the other hand, Apache Ant is a Java-based build automation tool released in the year 2000 and became the most popular java build tool at that time. Ant itself doesn't have a  built-in
support for dependency management. However, more recently, Apache Ivy was developed as a sub-project of the Apache Ant project. Later, this problem led to the creation of maven. It uses Xml files to define build scripts. Ant requires developers to write all the commands by themselves, which sometimes leads to huge XML build files that are hard to maintain. 

Next is presented a summary table.

| **Gradle**                |          vs          | **Ant**                |
| :---                      | :---                 | :---                   |
| Groovy or Kotlin          |                      | Java                   |
| build.gradle or build.gradle.kts  |              | build.xml              |
| Tasks                     |                      | Targets                |
| comfortable support for IDE integration |        | IDE integration is complex |
| simpler, less code, more noticeable |            | more code, more time, more complex |
| multi-project build       |                      | does not support multi-project build | 

Although is widely found feedback declaring Ant is a simple tool to use and learn, I consider exactly the opposite. I found very difficult to find information in the time we had to develop this assignment, and took me too much time to finalize the assignment. It was hard to create the correct build.xml and ivy files, to correctly configure the spring application. In short, I would always consider Gradle over Ant.

---

## 3. Implementation of the Alternative in Windows OS

Fist thing to do is to install Apache Ant and Apache Ivy on the local machine. For that access this [page](https://ant.apache.org/bindownload.cgi) and download a version of Ant. In my case I used Ant 1.9.15 version. Then, download Apache Ivy from this [page](https://ant.apache.org/ivy/download.cgi). I downloaded ivy-2.5.0 version. Extract both zip files and copy ivy jar and paste it into the lib folder inside apache-ant.

Now, system environment variables must be edited. Add "ANT_HOME" variable as the image shows.

![img_8.png](Images/img_8.png)

Next, add Ant to the Path variable.

![img_9.png](Images/img_9.png)

Finally, copy ivy-2.5.0 jar and paste it into C:\Users\"userName"\.ant\lib.

Now Ant is ready for use. Open the console and type `ant -version`. The output will show the version in use.

![img_10.png](Images/img_10.png)

To start coding the only thing left to do is to fetch the files needed from our project. We will need folder 'src' and files '.gitignore', 'package.json' and 'webpack.config'.

Now is time to create our project build.xml file. Next, is the code present in this file. It was based in an example removed from the internet.

```
<project

        xmlns:ivy="antlib:org.apache.ivy.ant"
        xmlns:spring-boot="antlib:org.springframework.boot.ant"
        name="Ant_Alternative" default="build">

    <property name="lib.dir" value="lib"/>
    <property name="build.dir" value="build"/>
    <property name="src.dir" value="src/main/java/"/>
    <property name="spring-boot.version" value="2.4.5"/>
    <property name="main-class" value="com.greglturnquist.payroll.ReactAndSpringDataRestApplication"/>


    <!-- paths used for compilation and run  -->

    <target name="resolve" description="--> retrieve dependencies with ivy">
        <ivy:retrieve pattern="lib/[conf]/[artifact]-[type]-[revision].[ext]"/>
    </target>

    <target name="classpaths" depends="resolve">
        <path id="compile.classpath">
            <fileset dir="lib/compile" includes="*.jar"/>
        </path>
    </target>

    <target name="init" depends="classpaths">
        <mkdir dir="build/classes"/>
    </target>

    <target name="compile" depends="init" description="compile">
        <javac srcdir="src/main/java/" destdir="build/classes" classpathref="compile.classpath"/>
    </target>

    <target name="build" depends="compile">
        <spring-boot:exejar destfile="build/myapp.jar" classes="build/classes">
            <spring-boot:lib>
                <fileset dir="lib/runtime"/>
            </spring-boot:lib>
        </spring-boot:exejar>
    </target>

    <target name="report" depends="resolve" description="--> generates a report of dependencies">
        <ivy:report todir="${build.dir}"/>
    </target>

    <target name="run" depends="resolve" description="compile and run the project">
        <java jar="build/Myapp.jar" fork="true"/>
    </target>

    <target name="clean" description="--> clean the project">
        <delete includeemptydirs="true">
            <fileset dir="${basedir}">
                <exclude name="src/**"/>
                <exclude name="build.xml"/>
                <exclude name="ivy.xml"/>
            </fileset>
        </delete>
    </target>

    <target name="clean-cache" description="--> clean the ivy cache">
        <ivy:cleancache/>
    </target>

</project>

```

If we try to build our project now we won't be successful. The ivy file is missing. So, I created a file ivy.xml with the next content.

```
<ivy-module version="2.0">

    <info organisation="org.springframework.boot" module="Ant_Alternative"/>
    <configurations>
        <conf name="compile" description="everything needed to compile this module"/>
        <conf name="runtime" extends="compile" description="everything needed to run this module"/>
    </configurations>
    <dependencies>
        <dependency org="org.springframework.boot" name="spring-boot-starter"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="org.springframework.boot" name="spring-boot-starter-thymeleaf"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="org.springframework.boot" name="spring-boot-starter-data-jpa"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="org.springframework.boot" name="spring-boot-starter-test"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="junit" name="junit" rev="4.12">
            <artifact name="junit" type="jar"/>
        </dependency>

        <dependency org="org.springframework.boot" name="spring-boot-starter-web"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="org.springframework.boot" name="spring-boot-devtools"
                    rev="${spring-boot.version}" conf="compile" />

        <dependency org="org.springframework.boot" name="spring-boot-starter-data-rest"
                    rev="${spring-boot.version}" conf="compile"/>

        <dependency org="org.springframework.boot" name="spring-boot-antlib" rev="${spring-boot.version}"/>

        <dependency org="com.h2database" name="h2" rev="1.4.200"/>
    </dependencies>

</ivy-module>

```

This file will allow to obtain all dependencies needed to compile the project.

Now, let use the command `ant build -lib lib/compile`. This command must be used first or else the build will be unsuccessful. With the flag `-lib <path>` we specified a path to search for jars and classes. Note: if the build fails run the command again. It should be successful at second attempt.

Finally, with the command `ant run` we initialize our server as the image shows.

![img_11.png](Images/img_11.png)

Use Control^C to terminate.

Commit and push changes to the repository.

### 3.1. Add task to copy Jar

Start by creating a bitbucket issue with the name "CopySnapshotInAnt". 

To accomplish this feature, we have to append the next code in the "build.xml" file.

``` 
<target name="copySnapshotJar" description="--> copies myapp.jar">
        <copy file="build/myapp.jar" todir="dist/"/>
    </target>
```

Use the command `ant copySnapshotJar` to perform the task. Next image shows before and after the task.

![img_12.png](Images/img_12.png)

To finalize, commit and push changes, referring and closing the corresponding issue.

---

### 3.2. Add task to delete files

Once again, start by creating a bitbucket issue with the name "DeleteFilesInAnt".

To accomplish this feature, we have to append again code in the "build.xml" file. First create the task as follows

``` 
<target name="deleteFiles" description="--> deletes content of built folder">
        <delete>
            <fileset dir="src/main/resources/static/built/" includes="**/**"/>
        </delete>
    </target>
```

The path from the variable 'dir' is the folder whose files we want to delete. In this case, no files will ever be generated into "built" directory. If we, for example, substitute this path with "dist", when executing the task folder "dist" would become empty.

![img_13.png](Images/img_13.png)

To complete our task we need to add a dependency in task "clean". So the final code of this target should look like the following

``` 
<target name="clean" description="--> clean the project" depends="deleteFiles">
        <delete includeemptydirs="true">
            <fileset dir="${basedir}">
                <exclude name="src/**"/>
                <exclude name="build.xml"/>
                <exclude name="ivy.xml"/>
            </fileset>
        </delete>
    </target>
```

With the command `ant clean -verbose` we can see that "deleteFiles" task occurs before "clean".

![img_14.png](Images/img_14.png)

Now, this assignment is done. Commit and push, referring and closing the corresponding issue.