# Class Assignment 5 (Part 2) Report

This file refers to the second part of the fifth class assignment.

The goal for this part is to create a pipeline in Jenkins to build the tutorial spring boot application, developed during ca2-Part2. In the pipeline should be defined six stages, that are: **Checkout**, **Assemble**, **Test**, **Javadoc**, **Archive** and **Publish Image**. In this last stage we should generate and push an image to docker hub. 

---

# 1. Analysis, Design and Implementation

At this moment you already have jenkins installed and we know how to run it. For that reason we can jump right up to the creation of the new pipeline.

## 1.1. Creating Pipeline

### 1.1.1. Credentials

Similarly to the previous paragraph, we already defined our own credentials to access our repository. As we will use the same repo there is no need to create new credentials. However, as we will push our image into Docker Hub we must create credentials to it.

Follow instructions in [ca5/Part1](../Part1/README.md) (1.2.1 - Credentials) if you are not sure how to create them. Remember to give them an unique ID otherwise will have conflict problems in the future.

### 1.1.2. Jenkinsfile

To run the pipeline and execute all the operations that we were ordered, we must create a Jenkinsfile where we indicate each one of them.

Create a new file inside Part2 and name it "Jenkinsfile".

Next is presented the information it should contain. Note that we will use the work performed in ca3/Part2 ([gradle-basic-demo](../../ca3/Part2/demo)).

````groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'LBS-bitbucket-credentials', url: 'https://bitbucket.org/1201921/devops-20-21/src/master'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'call ca3/Part2/demo/gradlew.bat -p ca3/Part2/demo clean assemble'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing'
                bat 'call ca3/Part2/demo/gradlew.bat -p ca3/Part2/demo test'
                junit 'ca3/Part2/demo/build/test-results/test/*.xml'
            }
        }
        stage ('Javadoc') {
            steps {
                echo 'Javadoking =)'
                bat 'call ca3/Part2/demo/gradlew.bat -p ca3/Part2/demo javadoc'
                javadoc javadocDir: 'ca3/Part2/demo/build/docs/javadoc/', keepAll: true
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca3/Part2/demo/build/libs/*.war'
            }
        }
        stage('Docker Publish') {
            steps {
                echo ('Dockering =)')
                script {
                    echo ('Docker Build...')
                    def dockerImage = docker.build ("<your workspace name>/devops-ca5-part2-repo:${env.BUILD_ID}", "ca3/Part2/demo")
                    echo ('Push to Repo...')
                    docker.withRegistry('https://registry.hub.docker.com', '<your docker credential>') {
                        dockerImage.push()
                    }
                }
            }
        }
    }
}
````

We have the six required stages. Let's explore each one of them:

- **Checkout**: is the same as in last assignment;

- **Assemble**: comparing to last assignment, the only difference is the path;

- **Test**: once again the path is the only difference;

- **Javadoc**: for this part we could use to ways, Javadoc or HTML Publisher plugins. In our case we will use Javadoc Plugin. This will be explained in a later section. What is important to retain for now is:
  
    - First we call the task Javadoc from Gradle;
    - Then, we call javadoc plugin, pass the javadoc report directory path in our repo, and tell jenkins we want to save all reports (one for each build).

- **Archiving**: we indicate the path and type of file we want to archive;

- **Docker Publish**: in order to create a Docker image, the Docker Pipeline plugin provides a build() method for creating a new image, from a Dockerfile in the repository, during a Pipeline run. So, we start by creating a script step. Inside it, we will have:

    - we define variable dockerImage that:
      
        - is equal to a docker build;
        - creates the remote repository and assigns a tag to the image;
        - fetches the dockerfile in the path indicated.
    
    - withRegistry() receives two parameters (the place to register the image, and credentials, in this order)
        
        - push image to Docker Hub.


### 1.1.4. Dockerfile

For this assignment we will create a very simple Dockerfile inside ca3/Part2/demo. It should be generated a docker image with Tomcat and the war file.

Your file should look like the following.

```
    FROM tomcat

    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://1201921@bitbucket.org/1201921/devops-20-21.git
    
    WORKDIR devops-20-21/ca3/Part2/demo
    
    RUN chmod u+x gradlew
    
    RUN ./gradlew clean build
    
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
    
    WORKDIR ../../../../..
    
    RUN rm -R tmp
    
    EXPOSE 8080
```

### 1.1.4. Creating the Pipeline

This process will be very similar to what we did in last assignment. In doubt, just follow last tutorial. Note that the only different will be the **Script Path**. In this case, our Jenkinsfile is located in ca5/Part2.

### 1.1.5. Plugins install

To make Jenkins perform as we expect the installation of two plugins are mandatory.

The first one is the **Javadoc Plugin**. This one will allow you to publish javadoc reports into Jenkins.

The second one is the **Docker Pipeline Plugin**. This allows building, testing, and using Docker images from Jenkins Pipeline projects. It introduces a `docker` global variable available without import to any flow script when the plugin is enabled.

Let's install them. It is a very simple job to accomplish. Access ![Alt](Images/img.png "Manage Jenkins") on the left menu, then **Manage Plugins** as shown in the next image.

![img_1.png](Images/img_1.png)

Go to **Available** menu and search for each plugin. Here is an example if we want to install Maven:

![img_2.png](Images/img_2.png)

We would search "Maven", select the checkbox on the desired plugin, and choose to "Install without restart" to make the plugin available right away.

After installation your plugins will be listed on the **Instaled** menu list.

![img_3.png](Images/img_3.png)

### 1.1.6. Running the pipeline

Is important to know that in order to run the pipeline successfully you should follow the previous steps thoroughly. It is also mandatory to have docker installed and running on your local machine.

To run the pipeline just access the pipeline you created in chapter 1.1.4. On the left click **Build Now**. 

If everything went well you will get something similar to the next image.

![img_4.png](Images/img_4.png)

Now, if you access this specific build you will be able to see it information. You should get the next result.

![img_5.png](Images/img_5.png)

We can assess that:

- Green: demo-0.0.1-SNAPSHOT.war was archived;
- Red: report of tests results for this build;
- Javadoc: Javadoc report for this build. This happens because we decided to keep all javadoc reports. If we don't, only one report would be available, which would be present inside the pipeline page. Everytime a new build is executed, the new report would override the previous.

If you browse to your docker hub workspace you will notice that a new remote repository was created with the name you have given in the jenkins file (in my case was devops-ca5-part2-repo:< BUILD_ID >).

![img_6.png](Images/img_6.png)

In it, we can see the image that refers to the build we just did. Each new build will generate a new image that will have as name the number of that build.

![img_7.png](Images/img_7.png)

---

# 2. Analysis of Buddy

Both Jenkins and Buddy are Continuous Integration / Continuous Delivery tools.

Jenkins requires installation and regular maintenance. With Buddy, in the other hand, we don't need to install, or do any maintenance because this problem is covered as part of our subscription. Nevertheless, Buddy offers a free service with 5 projects and 120 pipeline executions per month.

At customization level, Jenkins is very customizable, every person can create a new plugin, or we can select from a large list of available existing plugins. The problem is that these can collide with one another in unexpected ways or security issues can arise. The same does not happen with buddy. It comes prepackaged with several integrations and wide range of predefined actions developed by the company itself. This assures quality and no conflicts.

Pipeline creation is very similar between the two. It is possible to create through a GUI or a specific file. Jenkins uses Groovy syntax in its Jenkinsfile, and Buddy uses Yaml in a buddy.yaml file.

When comparing between the knowledge needed to run each tool, buddy is much simpler. With Jenkins a working knowledge is required to start in a sustainable way. Requires lots of time and effort to understand and correctly work with. Buddy has a very intuitive UI and, as the company refers, "allows even non-tech users to create and manage pipelines". Also, provides a lot of appealing documentation and engineer support. [Here](https://buddy.works/docs/quickstart/java) is an example of a quick start guide for java applications.

 **Jenkins** |  | **Buddy** |
| :---: | :---:| :---: |
| Open Source | | Not Open Source |
| Installation required| | No installation required | 
| Regular patching, tuning and cleaning | | No maintenance needed |
| Many Plugins and new Integrations | | No new integrations |
| Requires good understanding of Jenkins | | Practically no prior knowledge required |
| Scripted and UI driven Pipelines | | Scripted and UI driven Pipelines |
| Free | | Subscription |

In conclusion, I liked Buddy very much. Is very intuitive and simple, at least, to perform small projects like this one. The documentation is very complete and understandable. I didn't know this tool but without spending much effort and time, was able to create the desired pipeline. The only problem is that there is no way to archive the files we were asked to, or maybe I didn't find the way. Also, Buddy seems much quicker than Jenkins what is always good.

---

# 3. Implementation of Buddy

Start by creating an account. Access [Buddy web page](https://buddy.works/) and hit the button "Try Buddy for free". 

After this step, lets create our project. Press "Create a new Project". A new window will open. 

Choose your git hosting provider, and your repository name, as the image shows.
![img_8.png](Images/img_8.png)

After that your repository will be synchronized with your account. Now your project has the same structure as your repository. You are ready to create your pipeline.

Select "Add new pipeline" button. Enter the name, choose the way your pipeline will start running in "trigger mode" field, and corresponding branch.
![img_9.png](Images/img_9.png)

Let's start to create action, the equivalent to stages in Jenkins. Search Gradle and select it.
![img_10.png](Images/img_10.png)

The new window has a lot of customizations for you to choose. We will only use **Run**, **Environment** and **Action**.

![img_11.png](Images/img_11.png)

In the **Run** field enter the next code in the console.

``` 
    echo 'Assembling'
    cd ca3/Part2/demo
    gradle assemble
    echo 'Archiving'
    cp build/libs/* ../../../ca5/Part2/Buddy/Archives
```

Go to **Environment** field and change Gradle version to 6.8.3.

At last, in the **Action** menu change the action name, for example, to "Execute: gradle assemble", and save this action.

Two more gradle actions must be created, for test and javadoc tasks respectively.

The code is: 

- Testing
``` 
    echo 'Testing'
    cd ca3/Part2/demo
    gradle test
    echo 'Copying test report'
    cp build/test-results/test/*.xml ../../../ca5/Part2/Buddy/Tests
```

- Javadoc
``` 
    echo 'Javadocking'
    cd ca3/Part2/demo
    gradle javadoc
    echo 'Copying Javadoc'
    cp build/docs/javadoc/index-all.html ../../../ca5/Part2/Buddy/Javadoc
```

Change gradle version as before, and the action name accordingly.

Is important to refer that we could perform every gradle tasks in one action only. This mean was used to maintain some consistency with the work done at Jenkins.

At this point, create and publish a docker image is the only thing left to do. Add a new action, search "Docker" and select **Build Image**.
![img_12.png](Images/img_12.png)

Remember where our Dockerfile was located? In **Setup** insert "ca3/Part2/demo/Dockerfile" or browse it through GUI.

Go to **Options** field. Use the image as guide. The only difference is the docker account. Select the DockerHub account dropdown and add a new Docker Hub account.

![img_13.png](Images/img_13.png)

Note that we are giving a name to each tag. `$BUDDY_EXECUTION_ID` will assign to the tag the number of the corresponding build.

Save this action.

Now your pipeline should be similar to the next image.
![img_14.png](Images/img_14.png)

Just click "Run pipeline" to execute this pipeline. Select your last commit then run.
![img_16.png](Images/img_16.png)

During the process you can assess, in real time, how the job is evolving. Next example shows the aspect of a build.
![img_15.png](Images/img_15.png)

There are only two things to do: confirm that our files were copied and the Docker Image created.

Choose **Filesystem** and navigate into Buddy directory inside ca5/Part2. You will find three folders we created in the gradle actions. Inside of each one are the files we stored.

![img_17.png](Images/img_17.png)

Lastly, check your Docker Hub workspace. You will find that a new remote repository was created, and it has the image corresponding to the build you just performed.

![img_18.png](Images/img_18.png)
