# Class Assignment 5 (Part 1) Report

In this fifth assignment students should elaborate two tasks. This file refers to the first one.

In it, they were asked to perform a simple pipeline to the project gradle_basic_demo accomplished in ca2-Part1. This pipeline should include four stages, that are: **Checkout**, **Assemble**, **Test** and **Archive**. To do so, students must use Jenkins as a continuous integration (CI) tool.

---

## 1. Analysis, Design and Implementation

### 1.1. Jenkins installation

This tutorial shows how to install jenkins in your Windows based local machine using a [jenkins.war](https://get.jenkins.io/war-stable/2.289.1/jenkins.war) file.

After downloading it, open a command prompt window and navigate to the folder were this file is located. Run the command `java -jar jenkins.war`. This step could take a while. If, like me, you normally use the 8080 port, and want to use another one, append `--httpPort=<Port Number>` to the last command.

When finished, open a new browser page and access "localhost:< Port Number >". Wait until the **Unlock Jenkins** page appear. 

![img.png](Images/img.png)

Go to the console and copy the automatically-generated password. Next image exemplify where you can find and how it looks like.

![img_1.png](Images/img_1.png)

Paste it to the previous window and click **Continue**.

A **Customize Jenkins** window should appear. This allows us to install the plugins we want. As this is our first experience with Jenkins, choose "Install suggested plugins". Don't worry! You can install (or remove) additional Jenkins plugins at a later point in time via the **Manage Jenkins > Manage Plugins** page.

Finally, we must create an administrator user. Specify the details for the user, then, click **Save and Finish**.

Now, you can start using Jenkins. You can always access it through the address you used earlier. Just don't forget to run the command `java -jar jenkins.war --httpPort=<Port Number>` before you access it.

Here is an overview about jenkins with the difference that your dashboard is empty. Soon, that will change.

![img_2.png](Images/img_2.png)

---

## 1.2. Creating Pipeline

### 1.2.1. Credentials

As you know, our source code is hosted in a private git repository in Bitbucket. For that reason Jenkins won't be able to access it directly. At least, not at this moment. To be able to do it, we need to give access to it. We will do that by generating the corresponding credentials in Jenkins.

On the left menu, there are several fields.

- Click **Manage Jenkins > Manage Credentials**.
![img_3.png](Images/img_3.png)


- Then, click **Jenkins**.
![img_4.png](Images/img_4.png)
  

- Next, click **Global credentials (unrestricted)**.
![img_5.png](Images/img_5.png)
  

- And **Add Credentials** on the left.
  ![img_6.png](Images/img_6.png)
  

Insert your information as next image exemplifies. In the username and password fields use the actual credentials to access bitbucket. In the end click **OK**.
![img_7.png](Images/img_7.png)

In your credentials list you will see this new credential.

### 1.2.2. Jenkinsfile

A pipeline can be written in two ways: with declarative syntax where the code is written in a Jenkinsfile, or with scripted syntax, a more traditional and complex approach. In our case, we will use the first. For that matter we need to create a Jenkinsfile on our repository.

Navigate into ca5/Part1 folder, create a new file, and include the information as follows.

![img_8.png](Images/img_8.png)

How is this file structured? Well :

1. Execute this Pipeline or any of its stages, on any available agent;
   
2. Defines the "Checkout" stage;

3. Defines the "Assembles" stage;

4. Defines the "Test" stage;

5. Defines the "Archiving" stage - this will archive files generated during Assemble stage;

6. Steps from each stage;

7. Record and aggregate test results only if the current stage’s run has a different completion status from its previous run.

Is important to note this file is located in a different folder than the project itself. By default, Jenkins tries to execute the command with the information from his current directory. For that reason, when we write the command to execute certain operations we must call the folder where gradle wrapper is, and then the task. This will be structured as `'call <path till gradlew>/gradlew -p <same path> task'`.

You must commit changes to be able to run the pipeline later.

### 1.2.3. Creating the pipeline

On the jenkins page ([localhost:8080](localhost:8080)) you find on the left menu a field named "New Item". This will be the starting point to generate your first pipeline. Go ahead and click in it.

Insert the name for the pipeline and select the pipeline option as follows. Click **OK** when you are done.

![img_9.png](Images/img_9.png)

A new windows to configure our pipeline will open. Here,  we can configure build triggers that will start builds when a certain action occurs or periodically for example.

In the section "Pipeline" insert the required information as the following example.

![img_10.png](Images/img_10.png)

Only the Repository URL and Credentials fields will be different from the image.

Click **Save** to finish.

### 1.2.4. Running the pipeline

Now you made all the hard work, running the pipeline is as simple as clicking in ![Alt](Images/img_11.png "Build Now Icon") on the left menu. By doing it, you will trigger a new build. 

If all went well you'll get an output similar as the following image.

![img_12.png](Images/img_12.png)

All green is good. What you don't want is red. For example, next image shows that an error occurred in the Archiving stage.

![img_13.png](Images/img_13.png)

It is possible to analyse information about each build. Click in the build number on the **Build History** field, in the lower left corner. Now you have a bunch of fields (on the left) regarding this build.

For instance, to understand what went wrong on the last shown build click the **Console Output** field ( ![Alt](Images/img_14.png "Console Output Icon") ), and look for the error.

In the case of a successful build, when you click in the build, you will get something similar to the next image.

![img_15.png](Images/img_15.png)

Here you can get a lot of information, such as: 

  - The build artifacts that you archived;
  - The commit message and corresponding changes;
  - Who started the build;
  - The commit ID;
  - And the test results.