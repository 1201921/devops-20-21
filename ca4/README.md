# Class Assignment 2 Report

For this assignment students must use Docker to set up containers, to execute  the tutorial spring boot application. For that matter, two containers should be created, one for the web application, and other to the database.

---

## 1. Analysis, Design and Implementation

### 1.1. Docker Installation

Go to [this page](https://docs.docker.com/docker-for-windows/install/) and download docker desktop installer.exe . If your computer doesn't fulfill all requirements you can try docker toolbox, but be aware this is a non-recommended version.

Make sure you enable WSL 2 Windows and Hyper-V features like the following.

![img_4.png](Images/img_4.png)

It will take some time to unpack all files. After installation is done close the window and restart your machine.

![img_5.png](Images/img_5.png)

After reboot, open your console and use `docker -v` to confirm docker is installed.

![img_6.png](Images/img_6.png)

Let's check out initial state. In the Docker Desktop application you should see something similar to the following, an empty list.

![img.png](Images/img.png)

The green field  flagged in the image shows that Docker is running.

In the console, using the command `docker info` we will notice nothing was yet done or created, as supposed. 

![img_1.png](Images/img_1.png)

### 1.2. Preparing Project

Now, create and checkout to a new branch called "Docker-tut-basic". Open your terminal inside ca4 and use `git checkout -b Docker-tut-basic`.

Download this [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/) into the folder. 

Copy demo folder inside ca3-Part2 into ca4 using `cp -a ../ca3/Part2/demo/. demo`. We will use the work done in ca3/Part2/demo, where we performed several changes to use Vagrant, so, we don't need to repeat all the processes again.

There are three ways, that I know, to create containers. One is to get an image from a Docker registry, i.e., a place that stores and lets you distribute Docker images. After that you can perform changes on top of this image.

A second way is to create a dockerfile, based on an image, create a series of steps/commands that will configure our container. Every person that uses this file will obtain a container with the exact same configurations. Next is presented an example.

![img_10.png](Images/img_10.png)

The third way, that will be used in this project, is through a docker-compose.yml file. This will allow creating several containers at  (services), configure networking connections between containers, and volumes. The following image shows an example.

![img_11.png](Images/img_11.png)

It starts with the version (1, 2 or 3). At this moment version 1 is deprecated. 2 and 3 share the same structure, but they have different parameters. Containers must be declared under `Services` key, volumes can be declared under the `volumes` key, and networks under `networks` key.

Services are the containers to be created. `build:web` means that there will be a folder with name web that has a Dockerfile. The same to `build:db. 

In line 9 is the ip address of the web container. We can see that web container depends on db. This means that first is guaranteed that container db starts.

It is also possible to map the port that each container exposed. In line 6 for example, is possible to see the port 8080 exposed by the container will be transmitted in port 8080. The same is happening in line 15 and 16. The last line represents the range of addresses.

Docker has a nice feature relating to ip addresses. It converts automatically the name of the services in addresses. The ip address from containers are made available to all containers.

Now, open "web/Dockerfile" in the editor and make the next changes:

![img_3.png](Images/img_3.png)

- First line is our repository clone link.
- Then, we need to introduce a command to navigate to demo folder (line 19). 
- After, that we can build our project.
- Finally, copy the war file produced during build to a folder where tomcat will search it.

**note:** As the next image confirms, during first attempt to create web and db containers I was not able to perform gradlew clean build due to lack of permissions, so changes to the previous file had to be made.
![img_7.png](Images/img_7.png)

Normally, in the end of a docker file we need to introduce a command that tells the container what it should run when started. Because our container is based on a tomcat image there is no need to write such command. The image already tells that tomcat should be run.

In the case of the db Dockerfile its possible to see that, among other configurations, we download the h2 jar and execute it with the last command
`java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists`. This one is similar to last assignment.

Commit, push and merge changes to bitbucket as we have done in previous assignments.

### 1.3. Creating Containers

Now that we have done all the prep work we can create the two containers: web where the application will run, and db where we store our data. Make sure you have docker desktop application running.

- Get inside atb-docker-compose-spring-tut-demo folder.
- Use `docker-compose build --no-cache`. This will force build to run completely even if it is not the first time you run this command.
- Use `docker-compose up` to create containers. This command will do the following:
    - Builds images from Dockerfiles
    - Pulls images from registries
    - Creates and starts containers
    - Configures a bridge network for the containers
    - Handles logs
    
Running command `docker info` is possible to get the next information.

![img_12.png](Images/img_12.png)

We now have 2 containers existing, and both are running.

With the command `docker images` we can state that to images were also created.

![img_13.png](Images/img_13.png)

Use the command `docker ps` and you will notice the process of each container running. 

Finally, let's compare what is now showing on Docker Desktop Application.

![img_2.png](Images/img_2.png)

The two new containers were added to the list.

### 1.4. Accessing Web Application and H2 console

To access both web application and H2 console you will have to use your browser. Open a new window.

- Enter `localhost:8080/demo-0.0.1-SNAPSHOT` to access web Application;
- Digit `http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console` to access the login page to H2 console. In the URL field use `jdbc:h2:tcp://192.168.33.11:9092/./jpadb` as the image shows.

![img_14.png](Images/img_14.png)

You should have the same output as the following two images, respectively.

![img_9.png](Images/img_9.png)

![img_8.png](Images/img_8.png)

### 1.5. Publish images to docker hub

First, we have to resort to the `docker info` command again. This will output some necessary information. We will need the name of each container.

![img_13_2.png](Images/img_13_2.png)

Then, go to your Docker Hub account and create a repository for this purpose. You have to be registered in the website, so if you're not create an account. In my case I establish a repository named devops-ca4-repo for this matter.

In the terminal use the next commands:
- `docker tag atb-docker-compose-spring-tut-demo-1d63e00f58fe_db luisbelo91/devops-ca4-repo:db_container` that attributes the tag db_container to db container image
- `docker tag atb-docker-compose-spring-tut-demo-1d63e00f58fe_web luisbelo91/devops-ca4-repo:web_container` the tag web_container to web container image
- `docker push luisbelo91/devops-ca4-repo:web_container` and `docker push luisbelo91/devops-ca4-repo:db_container` pushes the tagged images to the remote repo

In the referred repository its possible to see the two images.

![img_15.png](Images/img_15.png)

### 1.6. Copy database file

To copy the database file from the container to a shared volume with our host machine, we have to run a shell inside the container. Remember that when creating the docker-compose.yml we declare such shared folder under volumes key. 

![img_16.png](Images/img_16.png)

- Use the command `docker exec -it [db-containerID] bash`. Instead of the id we can use also the complete name of the container.

- Then, `cp jpadb.mv.db ../data`.

Now inside folder data you should have a new file as follows.

![img_17.png](Images/img_17.png)

- Use `exit` to exit the container bash and return to your machine terminal.

If you prefer, you can stop your containers by using `docker stop [containerID or name]` for each one.

---

## 2. Analysis of Kubernetes

As we saw, Docker, that was created in 2013, is a containerization software that allows to isolate environments, build and run containers, store and share then using images through a container registry like Docker Hub, but there are more.

Kubernetes, created in 2014 by Google, can be used as an alternative or a complement to Docker. Well, for what I had read about both, Kubernetes appears to be more a complement than an alternative. It is actually an alternative to Docker Swarm. Either way, both encapsulate applications in an isolated and portable structure, the containers.

It is open source, and I consider it a container's manager. This means it reduces a big part of manual labor necessary to implement and scale applications in containers. K8s allows to group containers into pods, and helps to manage them easy and efficiently.

Kubernetes has several customizable possibilities, thus, becoming very flexible. However, this comes with a price: it becomes more complex and requires more time to learn and understand all its features.

---

## 3. Implementation of Kubernetes

Start by creating a new folder named Kubernetes inside ca4. Then, copy atb-docker-compose-spring-tut-demo-1d63e00f58fe directory into it. Download Kompose.exe from [here](https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-windows-amd64.exe) and save it in the folder you just copy-pasted.

- Execute the command `atb-docker-compose-spring-tut-demo-1d63e00f58fe>kompose-windows-amd64.exe convert`. This will convert docker-compose.yml into a readable file for kubernetes

You can see that some files with .yaml extension were created in this folder. 

In your IDE, or other editor as notepad or notepad ++, open this new files. In db/web-deployment.yaml we will change the link to the respective image in the remote repository.

![img_18.png](Images/img_18.png)

Open web/db-service.yaml and add the next line in each document.

![img_19.png](Images/img_19.png)

Finally, in default-networkpolicy.yaml file change information as follows.

![img_20.png](Images/img_20.png)

Next, we need to install minikube. Run the console as administrator and use the command `choco install minikube`. "Minikube is a tool that lets you run Kubernetes locally. Minikube runs a single-node Kubernetes cluster on your personal computer (including Windows, macOS and Linux PCs) so that you can try out Kubernetes, or for daily development work." (https://kubernetes.io/docs/tasks/tools/)

If you have curl already installed use `curl -LO https://dl.k8s.io/release/v1.21.0/bin/windows/amd64/kubectl.exe` to install kubectl, "the Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters." (https://kubernetes.io/docs/tasks/tools/)

Run the command `minikube --memory 3000 start` where memory flag ensure we will have enough memory available to proceed. 

We already made it before but just to be sure, run the command `docker login`. Use `kubectl apply -f .`, the flag -f means to apply the properties from the files we had changed. A normal error occurs with docker-compose.yml because this is not a readable file to k8s.

To check if everything is running insert the command `kubectl get pod -o wide`. After a while, you should get the next output.

![img_21.png](Images/img_21.png)

We can see we have two pods running, and they possess internal ip addresses. We must use the one from database pod in the application.properties. To do so:

- Run the command `kubectl exec -it [pod-name] -- bash`. This will get you inside web pod

- `apt install nano` in order to be able to edit the referred file

- Use `nano src/main/resources/application.properties` to open it and change the ip address to the one you see in last image. In my case is 172.17.0.3

- Create a new war file using `./gradlew clean build`

- Copy it to the folder where tomcat will search with the command `cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps`. This will override the existing file.

- Exit the pod with `exit` command

Now, to access web application and h2 console we have to expose the deployments in each pod. So,

- `minikube service web` will open a new tab in the browser showing the status "404 - Not found". Just append the name of the war file on the address like the image shows.

![img_23.png](Images/img_23.png)

- Use `minikube service db` and use the URL we had changed in the application.properties earlier.

![img_22.png](Images/img_22.png)

